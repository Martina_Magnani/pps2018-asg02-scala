package u03Lab

// An Optional data type
sealed trait Option [A]

object Option {

  case class None [A]() extends Option [A]
  case class Some [A](a: A) extends Option [A]

  def isEmpty [A](opt: Option [A]): Boolean = opt != None ()

  def getOrElse [A, B >: A](opt: Option [A], orElse : B): B = opt match {
    case Some(a) => a
    case _ => orElse
  }

  // A function that keeps the value only if it satisfies the given predicate
  def filter [A] (opt: Option [A] ) (f: A => Boolean): Option[A] = opt match {
    case Some(a) if f(a) => Some(a)
    case Some(a) if !f(a) => None()
    case _ => None()
  }

  // A function that trasform the value (if present)
  def map [A, B] (opt: Option[A]) (f: A => B): Option[B] = opt match {
    case Some(a) => Some(f(a))
    case _ => None()
  }

  // Combines two Options into another Option (if any input is None, the output is None)
  def map2 [A <: C, B <: C, C] (opt1: Option[A], opt2: Option[B]) (combine: (A,B) => C): Option[C] = (opt1, opt2) match {
    case (Some(a), Some(b)) => Some(combine(a, b))
    case _ => None()
  }

}

object TryOption extends App {

  import Option._

  println(filter(Some(5))(_ > 2))
  println(filter(Some(5))(_ > 8))

  println(map(Some(5))(_ > 2))
  println(map(Some(5))(_ > 8))
  println(map(None[Int])(_ > 8))

  println(map2(Some(4), Some(5))(_ + _))

}
