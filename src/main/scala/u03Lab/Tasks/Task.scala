package u03Lab.Tasks

import u03Lab.List
import u03Lab.List._
import u03Lab.Person
import u03Lab.Person.{Student, Teacher}

object Task extends App {

  // Functional composition
  def compose(f: Int => Int, g: Int => Int): Int => Int = {
    x => f(g(x))
  }

  compose(_ + 1, _ * 2)(5)

  // N-th Fibonacci number
  def fib(n:Int):Int = n match {
    case 0 => 0
    case 1 | 2 => 1
    case _ => fib(n-1) + fib (n-2)
  }

  println(fib(0), fib(1), fib(2), fib(3), fib(4), fib(5), fib(6))

  // Create a nice string representation for a Person
  def personToString(p: Person): String = p match {
    case Student(n, y) => n + ": " + y.toString
    case Teacher(n, c) => n + ": [" + c + "]"
  }

  println(personToString(Teacher("Mirko", "PPS")))

  println(personToString(Student("Mario", 2016)))

  // Recursive way: create a list containing only the courses of teachers in input list
  def getTeachersCoursesRecursive(l: List[Person]): List[String] = l match {
    case Cons(h, t) => h match {
      case Teacher(n, c) => Cons(c, getTeachersCoursesRecursive(t))
      case _ => getTeachersCoursesRecursive(t)
    }
    case Nil() => Nil()
  }

  // Functional way: create a list containing only the courses of teachers in input list
  def getTeachersCourses(l: List[Person]): List[String] =  {
    map(filter(persons) {
      case Teacher(_, _) => true
      case _ => false
    }) {
      case Teacher(_, c) => c
    }
  }

  val persons: List[Person] = Cons(Teacher("Mirko", "PPS"),
    Cons(Student("Mario", 2016),
      Cons(Student("Martina", 2016),
        Cons(Teacher("Alessandro", "PCD"),
          Cons (Teacher("Antonio", "LSS"),
            Cons(Teacher("Mario", "LCMC"),
              Nil()))))))

  println(getTeachersCoursesRecursive(persons))

  println(getTeachersCourses(persons))
}
