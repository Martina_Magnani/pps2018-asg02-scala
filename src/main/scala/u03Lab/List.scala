package u03Lab

import Option.{None, Some}

sealed trait List[E]

object List {

  case class Cons [E]( head : E, tail : List [E]) extends List [E]
  case class Nil [E ]() extends List [E]

  def length [E](l: List [E]): Int = l match {
    case Cons (h, t) => 1 + length (t)
    case _ => 0
  }

  def sum (l: List [ Int ]): Int = l match {
    case Cons (h, t) => h + sum(t)
    case _ => 0
  }

  def append [A <: C, B <: C, C]( l1: List [A], l2: List [B]): List [C] = (l1 , l2) match {
    case ( Cons (h, t), l2) => Cons [C](h, append (t, l2))
    case (l1 , Cons (h, t)) => Cons [C](h, append (l1 , t))
    case _ => Nil ()
  }

  // Remove the first n elements from the list
  def drop [A] (l: List[A], n: Int) : List[A] = (l, n) match {
    case (Cons (h, t), n) if n > 0 => drop(t, n-1)
    case (Cons (h, t), n) if n == 0 => Cons(h,t)
    case (Nil(), n) => Nil()
  }

  // Map the elements of a list given a function
  def map[A,B](l: List[A])(f: A => B): List[B] = l match {
    case Cons(h,t) => Cons(f(h), map(t)(f))
    case Nil() => Nil()
  }

  // Filter the elements of a list given a Boolean function
  def filter [A] (l:List[A]) (f: A => Boolean) : List[A] = l match {
    case Cons(h,t) if f(h) => Cons(h,filter(t)(f))
    case Cons(h,t) if !f(h) => filter(t)(f)
    case Nil() => Nil()
  }

  // Find the maximum integer of a list
  def max(l: List[Int]): Option[Int] = {
    def _max (l: List[Int], max:Int) : Int = (l, max) match {
      case (Cons(h, t), max) if h > max => _max(t, h)
      case (Cons(h, t), max) if h <= max => _max(t, max)
      case (Nil(), max) => max
    }
    l match {
      case Nil() => None()
      case Cons(h,t) => Some(_max(t, h))
    }
  }

  // Fold left lists by 'accumulating' elements through a binary operator
  def foldLeft [A, B] (l: List[A])(default: B)(f: (B, A) => B): B = l match {
    case Cons(h, t) => foldLeft(t)(f(default, h))(f)
    case _ => default
  }

  // Fold right lists by 'accumulating' elements through a binary operator
  def foldRight [A, B] (l: List[A])(default: B)(f: (B, A) => B): B = l match {
    case Cons(h, t) => f(foldRight(t)(default)(f), h)
    case _ => default
  }

}

object TryList extends App {
  import List._

  val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))
  println(foldLeft(lst)(0)(_+_)) // 16
  println(foldRight(lst)("")(_+_)) // "5173"
}