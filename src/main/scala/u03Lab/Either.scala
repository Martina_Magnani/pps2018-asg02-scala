package u03Lab

import Option._

sealed trait Either [L, R]

object Either {

  case class Left [L, R](l: L) extends Either[L,R]
  case class Right [L, R](r: R) extends Either[L, R]

  def getLeft[L,R] (e: Either[L,R]): Option[L] = e match {
    case Left(l) => Some(l)
    case _ => None()
  }

  def getRight[L,R] (e: Either[L,R]): Option[R] = e match {
    case Right(r) => Some(r)
    case _ => None()
  }

  def isLeft[L,R] (e: Either[L,R]): Boolean = e match {
    case Left(l) => true
    case _ => false
  }

  def isRight[L,R] (e: Either[L,R]): Boolean = e match {
    case Right(r) => true
    case _ => false
  }

  def mapLeft[L,R,B] (e: Either[L,R])(f: L => B): Either[B,R] = e match {
    case Left(l) => Left(f(l))
    case Right(r) => Right(r)
  }

  def mapRight[L,R,B] (e: Either[L,R])(f: R => B): Either[L,B] = e match {
    case Right(r) => Right(f(r))
    case Left(l) => Left(l)
  }

  def swap[L,R] (e: Either[L,R]): Either[R,L] = e match {
    case Left(l) => Right(l)
    case Right(r) => Left(r)
  }
}

object TryEither extends App {
  import Either._

  val l: Either[String, Int] = Left("Flower")
  val r: Either[String, Int] = Right(4)

  println(mapLeft(l)(_.length)) // Left(6)
  println(mapLeft(l)(_ + " Power")) // Left("Flower Power")
  println(mapRight(l)(_ + "Power")) // Left("Flower")

  println(mapRight(r)(_ + " x 4 = 16")) // Right("4 x 4 = 16")
  println(mapRight(r)(_ * 4)) // Right(16)
  println(mapLeft(r)(_ * 4)) // Right(4
}